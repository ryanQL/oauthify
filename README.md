# OAuthify

  OAuth2 Service provider with JWT support.

## Set up

  Rename the .env_example to .env and assign values to the following environment variables:

  DB_USER - postgres database user.
  DB_PASSWORD - postgres database user password.
  DB_NAME - database name.
  DB_HOST - database host.
  JWT_SECRET - Secret used to generate the JWT tokens.

### Run migrations

  ``` export $(cat oauth-service.prod.env | xargs) && rails db:migrate ```

### Run the app

  ``` export $(cat oauth-service.prod.env | xargs) && rails s -p 5000 ```

### Sign Up

  [Register a new user](http://localhost:5000/users/sign_up)

   Email: user@example.com
   
   Password: doorkeeper

### Register a new OAuth Application using the admin panel

  [New OAuth Application](http://localhost:5000/oauth/applications/new).

  Name: Test App
  Redirect URI: urn:ietf:wg:oauth:2.0:oob
  Confidential: true (default)
  Scopes: blank (default)

### Retrieve an access token using
```
  curl -F grant_type=password \
  -F username=user@example.com \
  -F password=doorkeeper \
  -X POST http://localhost:5000/oauth/token
```
It should output a JWT token:

```
{
  "access_token":"eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpc3MiOiJvYXV0aF9hcGkiLCJpYXQiOjE1MzYwMTM4OTMsImp0aSI6IjNjN2MxMzIwLTg3ZDAtNDY4NS05ZjU4LTVhZGVjY2Y4Y2MzOSIsInVzZXIiOnsiaWQiOjEsImVtYWlsIjoidXNlckBleGFtcGxlLmNvbSJ9fQ.Hia-rZK4n_-GxdP3XP4Q8LKm9mrenWUGR8jK3CSzvc0",
  "token_type":"Bearer",
  "expires_in":6699,
  "created_at":1536013893
}
```

[More info about grant types and curl commands](https://github.com/doorkeeper-gem/doorkeeper/wiki/API-endpoint-descriptions-and-examples)
