require_relative 'boot'

require 'rails/all'

Bundler.require(*Rails.groups)

module OauthApi
  class Application < Rails::Application
    config.active_job.queue_adapter = :sidekiq
    config.application_name = "OAuthify"
    config.load_defaults 5.2
  end
end
