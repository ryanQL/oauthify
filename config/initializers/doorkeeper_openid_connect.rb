Doorkeeper::OpenidConnect.configure do
  issuer 'http://localhost:5000'

  signing_key <<-EOL
-----BEGIN RSA PRIVATE KEY-----
MIIEowIBAAKCAQEApnz9WxkYaniOH8SH2xEfr7meINYt0XLOlFO4j75JBtJ2FsgT
WbvrgQG2G3HWYL70uWD/NMM68sxbM7DIk5rK00d3/OFzi33jWZwYoMd/OIeRdDaH
8P1kGcJ14PBamioDAIh6beIowPK01tT6J+89q/kh+C0RAMHKVxUAY2yrH3I5Pp9J
gpS/dzSHgxSd1BMFgEpsvEs9inx5X76zWGnXc015Ve5ltH9yS9/lYGPaeyYd8PDY
zu4e6dH05FPRIzZISXjXsxfm/OJMVFRNLFVT42fiYyUt3xAJf57PgI8iKKP2mHZL
UrQI1HqDJg4Mdkb+dL5OoXM6pQbIUA/G/i6PfwIDAQABAoIBAFjl0Jq+Ea14ZdtM
o4Yz5Il+QZMjCiAWf3vwIFKQtomwA2IyvzJhB2VBNq384Q2TqTT7OApCXM2clO5K
7S7zrm2WcdlTwNCxDZ6hnPzsyV+z89Tr8Taa9sJSdYMAlEBRQn0cYtMduckdT7aO
zmxYcMPNKXhNCBExaQuKqURtmAoXpOTiVNm+0KOlwS7YXSJF0t9TohTmJapnEtb4
pp96OQ1DEleKRXCj5S0qNv6zCtn6cWeb/XfQp4/E3/TrFsDpfMRpock17rYmXxP0
o+cAPVocOL0FylxboQBkNGsIoFwFxL0RwaSwNZdAPDNK+0mWOiXmN/yV+qewC3TN
qsp3JGECgYEA2s87yqZ6UlhpqV3aK5tYJnlIEGVrSkD/J9SDiJVBnqC0juzqNeGX
K1rp13JYmORQf6KDKEyUjQIubWhODCXL/ryQr6xWZ3QQ9+tVYglOJQI42qhCRGDs
j9D5OwM+TGmdaIi4mg48IXZXXbyAEqrSuDKW9oH6rA5H0NNdMraWCYcCgYEAwskr
rg8fzsPFBhz3Djpn0RrUpJVvhV/XJO7yFnfUg4DhwIgWmaTPRiD4XW2va5KrdMSy
J7IEXPOtTgecKi2ubTUD2f4gJa8zpD/ibs32QJwkjFKInRLR1q1ddSBE4sNa//zR
wtYMdzl1XLikPXHXppLqIO25urf30JOsvpABaEkCgYEA10fNrDTfZOVYECsm3ICa
2j+5h2hBh20rniSYxWkUht3WyEiQpzKtaRqpuC/ou4TnLqzWHnA6OAJNa5eUMp99
dcRRFoaY/HBVwZbpCuvc1sy442loyZ4TIeyZSlaGlOfU12StO+WFg+bLTo6H89Xe
PRyJKz/3QhFK10seByddt0ECgYB7A6tnULvoy/BpDO/l93LLN2Ol0Y5NsBpig9k1
4VW/+ywXeJq1S8VvZ7ES6AuKJ2XK59IPibiBr476oYm5hYIMDBgzmu9YfU5i48mv
6rkjUrkuOY13jYQiTPih2NBIWdHlqxgaMS5MOWhqN13aRERKOaxNQXUze62w9yJ4
ct/AqQKBgCzdcmkyMN4TmG9iwLhzn2Ss5Ugrf1doKJY+MBxCvYSHYOlJDrl3K+A7
Pwz/OzheZLEz1PwNs/1T8SSURxpEU3BX6Pyvwo/vxVjZCd5i1qzezqxya44iqT0s
K2lNvsdJXJZC2Uv190NEb7u4t6TBdtkiYfvZiyPKIE481shwrGU8
-----END RSA PRIVATE KEY-----
EOL

  subject_types_supported [:public]

  resource_owner_from_access_token do |access_token|
    User.find_by(id: access_token.resource_owner_id)
  end

  auth_time_from_resource_owner do |resource_owner|
    resource_owner.current_sign_in_at
  end

  reauthenticate_resource_owner do |resource_owner, return_to|
    store_location_for resource_owner, return_to
    sign_out resource_owner
    redirect_to new_user_session_url
  end

  subject do |resource_owner, application|
    # resource_owner.id
    # or if you need pairwise subject identifier, implement like below:
    Digest::SHA256.hexdigest("#{resource_owner.id}#{URI.parse(application.redirect_uri).host}#{Rails.application.secrets.secret_key_base}")
  end

  # Protocol to use when generating URIs for the discovery endpoint,
  # for example if you also use HTTPS in development
  # protocol do
  #   :https
  # end

  # Expiration time on or after which the ID Token MUST NOT be accepted for processing. (default 120 seconds).
  expiration 600

  # Example claims:
  # claims do
  #   normal_claim :_foo_ do |resource_owner|
  #     resource_owner.foo
  #   end

  #   normal_claim :_bar_ do |resource_owner|
  #     resource_owner.bar
  #   end
  # end

  claims do
    claim :email do |resource_owner|
      resource_owner.email
    end

    claim :full_name do |resource_owner|
      "#{resource_owner.first_name} #{resource_owner.last_name}"
    end

    claim :preferred_username, scope: :openid do |resource_owner, application_scopes, access_token|
      # Pass the resource_owner's preferred_username if the application has
      # `profile` scope access. Otherwise, provide a more generic alternative.
      application_scopes.exists?(:profile) ? resource_owner.preferred_username : "summer-sun-9449"
    end

    claim :groups, response: [:id_token, :user_info] do |resource_owner|
      resource_owner.groups
    end
  end
end
