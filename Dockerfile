FROM ruby:alpine

ENV APP /oauth-service

RUN apk add --update build-base postgresql-dev nodejs git yarn
RUN apk add tzdata

WORKDIR $APP
ADD . .

RUN gem install bundler
RUN bundle install
RUN yarn install

EXPOSE 8080
CMD ["rackup", "-p", "8080", "--host", "0.0.0.0"]
