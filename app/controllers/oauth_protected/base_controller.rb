module OAuthProtected
  class BaseController < ::ActionController::Base
    before_action :doorkeeper_authorize! # Require access token for all actions
    private
    def current_resource_owner
      User.find(doorkeeper_token.resource_owner_id) if doorkeeper_token
    end
  end
end
