module OAuthProtected
  class UsersController < BaseController
    # GET /oauthorized/me(.:format)
    def me
      user_json = current_resource_owner.as_json
      render json: user_json, status: 200
    end
  end
end
