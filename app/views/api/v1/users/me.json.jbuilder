json.id current_resource_owner.id
json.email current_resource_owner.email
json.name [current_resource_owner.first_name, current_resource_owner.last_name].join(' ')
json.first_name current_resource_owner.first_name
json.last_name current_resource_owner.last_name
json.permalink current_resource_owner.permalink
json.avatar_url gravatar_image_url(current_resource_owner.email, size: 40)
