#!/bin/bash
export $(cat .env_example | xargs) && rails db:migrate
export $(cat .env_example | xargs) && rails s -p 5000
