# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 2018_07_24_093012) do

  # These are extensions that must be enabled in order to support this database
  enable_extension "plpgsql"

  create_table "friendly_id_slugs", id: :serial, force: :cascade do |t|
    t.string "slug", null: false
    t.integer "sluggable_id", null: false
    t.string "sluggable_type", limit: 50
    t.string "scope"
    t.datetime "created_at"
    t.index ["slug", "sluggable_type", "scope"], name: "index_friendly_id_slugs_on_slug_and_sluggable_type_and_scope", unique: true
    t.index ["slug", "sluggable_type"], name: "index_friendly_id_slugs_on_slug_and_sluggable_type"
    t.index ["sluggable_id"], name: "index_friendly_id_slugs_on_sluggable_id"
    t.index ["sluggable_type"], name: "index_friendly_id_slugs_on_sluggable_type"
  end

  create_table "oauth_access_grants", force: :cascade do |t|
    t.integer "resource_owner_id", null: false
    t.bigint "application_id", null: false
    t.string "token", null: false
    t.integer "expires_in", null: false
    t.text "redirect_uri", null: false
    t.datetime "created_at", null: false
    t.datetime "revoked_at"
    t.string "scopes"
    t.index ["application_id"], name: "index_oauth_access_grants_on_application_id"
    t.index ["token"], name: "index_oauth_access_grants_on_token", unique: true
  end

  create_table "oauth_access_tokens", force: :cascade do |t|
    t.integer "resource_owner_id"
    t.bigint "application_id"
    t.string "token", null: false
    t.string "refresh_token"
    t.integer "expires_in"
    t.datetime "revoked_at"
    t.datetime "created_at", null: false
    t.string "scopes"
    t.string "previous_refresh_token", default: "", null: false
    t.index ["application_id"], name: "index_oauth_access_tokens_on_application_id"
    t.index ["refresh_token"], name: "index_oauth_access_tokens_on_refresh_token", unique: true
    t.index ["resource_owner_id"], name: "index_oauth_access_tokens_on_resource_owner_id"
    t.index ["token"], name: "index_oauth_access_tokens_on_token", unique: true
  end

  create_table "oauth_applications", force: :cascade do |t|
    t.string "name", null: false
    t.string "uid", null: false
    t.string "secret", null: false
    t.text "redirect_uri", null: false
    t.string "scopes", default: "", null: false
    t.boolean "confidential", default: true, null: false
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["uid"], name: "index_oauth_applications_on_uid", unique: true
  end

  create_table "oauth_openid_requests", force: :cascade do |t|
    t.integer "access_grant_id", null: false
    t.string "nonce", null: false
  end

  create_table "services", force: :cascade do |t|
    t.bigint "user_id"
    t.string "provider"
    t.string "uid"
    t.string "access_token"
    t.string "access_token_secret"
    t.string "refresh_token"
    t.datetime "expires_at"
    t.text "auth"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["user_id"], name: "index_services_on_user_id"
  end

  create_table "users", id: :serial, force: :cascade do |t|
    t.string "first_name", limit: 255, default: ""
    t.string "last_name", limit: 255, default: ""
    t.string "email", limit: 255, default: ""
    t.string "encrypted_password", limit: 255, default: ""
    t.string "reset_password_token", limit: 255
    t.datetime "reset_password_sent_at"
    t.datetime "remember_created_at"
    t.integer "sign_in_count", default: 0
    t.datetime "current_sign_in_at"
    t.datetime "last_sign_in_at"
    t.string "current_sign_in_ip", limit: 255
    t.string "last_sign_in_ip", limit: 255
    t.string "confirmation_token", limit: 255
    t.datetime "confirmed_at"
    t.datetime "confirmation_sent_at"
    t.string "unconfirmed_email", limit: 255
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.integer "role", default: 0, null: false
    t.string "invitation_token", limit: 60
    t.datetime "invitation_sent_at"
    t.datetime "invitation_accepted_at"
    t.integer "invitation_limit"
    t.integer "invited_by_id"
    t.string "invited_by_type", limit: 255
    t.datetime "locked_at"
    t.string "university_email", limit: 255
    t.string "university_name", limit: 255
    t.string "taught_subject", limit: 255
    t.text "application_reason"
    t.text "referral_emails"
    t.integer "application_status"
    t.text "general_interests"
    t.string "gender", limit: 255
    t.string "post_code", limit: 255
    t.integer "university_id"
    t.integer "department_id"
    t.string "subject_area", limit: 255
    t.integer "notification_intensity", default: 1
    t.boolean "approve_comments", default: false
    t.string "permalink", limit: 255
    t.boolean "prompted"
    t.boolean "oauthify_account"
    t.text "profile_options", default: "--- {}\n\n"
    t.datetime "deleted_at"
    t.string "recover_token", limit: 255
    t.integer "notifications_count", default: 0, null: false
    t.string "time_zone", limit: 255, default: "London"
    t.date "date_of_born"
    t.string "avatar", limit: 255
    t.integer "users_followers_count", default: 0, null: false
    t.integer "followers_users_count", default: 0, null: false
    t.integer "messaging_setting", default: 0
    t.boolean "fill_profile_notification", default: true
    t.string "twitter_username", limit: 255
    t.string "facebook_username", limit: 255
    t.string "linked_in_username", limit: 255
    t.string "website_url", limit: 255
    t.text "bio"
    t.string "country", limit: 255
    t.string "region", limit: 255
    t.integer "blog_posts_count", default: 0
    t.string "company", limit: 255
    t.string "position", limit: 255
    t.integer "industry_id"
    t.datetime "invitation_created_at"
    t.boolean "signup_completed", default: false
    t.string "signup_progress", limit: 255
    t.text "testimonial"
    t.boolean "verified", default: false
    t.integer "followers_notification_intensity", default: 3
    t.string "promoted_roles", limit: 255
    t.integer "add_to_hubs", default: 3
    t.string "authentication_token", limit: 255
    t.integer "featured_image_id"
    t.string "youtube_username", limit: 255
    t.string "youtube_video_url", limit: 255
    t.text "contact_address"
    t.string "telephone", limit: 255
    t.integer "dashboard_notifications_count", default: 0
    t.integer "hub_notifications_count", default: 0
    t.integer "abode_notifications_count", default: 0
    t.string "hub_post_footer", limit: 255
    t.integer "analytics", default: 0
    t.string "s3_avatar", limit: 255
    t.string "stripe_id", limit: 255
    t.integer "job_sector_id"
    t.boolean "push_notifications"
    t.string "s3_banner", limit: 255
    t.index ["authentication_token"], name: "index_users_on_authentication_token"
    t.index ["confirmation_token"], name: "index_users_on_confirmation_token", unique: true
    t.index ["department_id"], name: "index_users_on_department_id"
    t.index ["email"], name: "index_users_on_email"
    t.index ["industry_id"], name: "index_users_on_industry_id"
    t.index ["invitation_token"], name: "index_users_on_invitation_token"
    t.index ["invited_by_id"], name: "index_users_on_invited_by_id"
    t.index ["permalink"], name: "index_users_on_permalink"
    t.index ["recover_token"], name: "index_users_on_recover_token", unique: true
    t.index ["reset_password_token"], name: "index_users_on_reset_password_token", unique: true
    t.index ["university_id"], name: "index_users_on_university_id"
  end

  add_foreign_key "oauth_access_grants", "oauth_applications", column: "application_id"
  add_foreign_key "oauth_access_grants", "users", column: "resource_owner_id"
  add_foreign_key "oauth_access_tokens", "oauth_applications", column: "application_id"
  add_foreign_key "oauth_access_tokens", "users", column: "resource_owner_id"
  add_foreign_key "oauth_openid_requests", "oauth_access_grants", column: "access_grant_id"
  add_foreign_key "services", "users"
end
